//
//  Countries.swift
//  TP2_Blanchard_Leopold
//
//  Created by mbds on 11/04/2021.
//

import Foundation

let countries = [
    Country(isoCode: "at", name: "Austria", continent: "Europe"),
    Country(isoCode: "be", name: "Belgium", continent: "Europe"),
    Country(isoCode: "de", name: "Germany", continent: "Europe"),
    Country(isoCode: "el", name: "Greece", continent: "Europe"),
    Country(isoCode: "fr", name: "France", continent: "Europe"),
    Country(isoCode: "ht", name: "Haiti", continent: "America"),
    Country(isoCode: "us", name: "USA", continent: "America"),
    Country(isoCode: "cn", name: "Canada", continent: "America"),
]
