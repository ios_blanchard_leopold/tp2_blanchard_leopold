//
//  Country.swift
//  TP2_Blanchard_Leopold
//
//  Created by mbds on 11/04/2021.
//

import Foundation

struct Country {
    var isoCode: String
    var name: String
    var continent: String
}
