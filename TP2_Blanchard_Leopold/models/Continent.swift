//
//  Continent.swift
//  TP2_Blanchard_Leopold
//
//  Created by mbds on 11/04/2021.
//

import Foundation


struct Continent {
    var nom: String
    var countries: [Country]
}
